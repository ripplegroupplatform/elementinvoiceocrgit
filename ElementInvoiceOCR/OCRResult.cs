﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementInvoiceOCR
{
    public class OCRResult
    {
        public float confidence;
        public string result;


        public OCRResult(float _confidence, string _result)
        {
            confidence = _confidence;
            result = _result;
        }

    }
}
