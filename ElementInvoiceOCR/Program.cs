using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using ImageMagick;
using iText;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Utils;
using iText.Layout;
using Tesseract;

namespace ElementInvoiceOCR
{
    public static class Progam
    {
        public enum Vendor { NONE = 0, TOPCO = 162, GREGG = 87, BADGER = 875 };
        public enum VendorInvoiceEmailStatus { NEW = 1, SUCCESS = 2, FAILED = 3, NO_VENDOR = 4 };

        public enum FailedReasons { CANT_MATCH_PO = 1, PO_AMOUNT_MISMATCH = 2, PO_VENDOR_MISMATCH = 3, PO_MATCHED = 4, DUPLICATE = 5 };

        public static int countryID = Convert.ToInt32(ConfigurationManager.AppSettings["countryID"]);

        public static void Main()
        {
          

            try
            {
                while (1 == 1)
                {

                    Console.WriteLine("DB start");

                    using (LocalDatabase db = new LocalDatabase(countryID))
                    {
                        string sql = "select * from vendorInvoiceEmails where statusID = " + Convert.ToInt32(VendorInvoiceEmailStatus.NEW) + " and isdeleted = 0 ";

                        Console.WriteLine("sql" + sql);

                        db.myCommandLocal.CommandText = sql;

                        db.myReaderLocal = db.myCommandLocal.ExecuteReader();

                        int emailID = 0;
                        int invoiceID = 0;
                        int vendorID = 0;

                        while (db.myReaderLocal.Read())
                        {
                            try
                            {
                                emailID = Convert.ToInt32(db.myReaderLocal["id"].ToString());
                                invoiceID = Convert.ToInt32(db.myReaderLocal["invoiceID"].ToString());
                                vendorID = Convert.ToInt32(db.myReaderLocal["vendorID"].ToString());

                                Console.WriteLine("Starting " + invoiceID);

                                try
                                {
                                    run(emailID, invoiceID, vendorID);
                                }
                                catch (Exception e)
                                {
                                    using (LocalDatabase ldb = new LocalDatabase(countryID))
                                    {
                                        sql = " update vendorInvoiceEmails set statusID = " + Convert.ToInt32(VendorInvoiceEmailStatus.FAILED) + ", exceptionMsg = '" + e.ToString() + "' where id = " + emailID;
                                        ldb.insertOrUpdate(sql);
                                    }


                                }

                                Thread.Sleep(10000);
                            }
                            catch(Exception e)
                            {
                                Console.WriteLine("No invoice found:" + invoiceID);
                            }
                        }

                    }

                    System.Threading.Thread.Sleep(900000);
                }

            }
            catch (Exception e)
            {
                sendEmail("Element OCR error1", e.ToString());
            }

        }


        static void run(int emailID, int invoiceID, int vendorID)
        {
            //string orig = "C:\\dl\\inv01gdl063-053675_25.pdf";

            Vendor v = Vendor.NONE;

            if (vendorID == 87)
                v = Vendor.GREGG;
            else if (vendorID == 162)
                v = Vendor.TOPCO;
            else if (vendorID == 875)
                v = Vendor.BADGER;

            if (v != Vendor.NONE)
            {

                string filePath = "";

                string sql = "select * from invoiceImages where invoiceID =  " + invoiceID;


                using (LocalDatabase ldb = new LocalDatabase(countryID))
                {
                    ldb.myCommandLocal.CommandText = sql;

                    ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                    if (ldb.myReaderLocal.Read())
                    {
                        filePath = ldb.myReaderLocal["filePath"].ToString();
                    }
                }

                Console.WriteLine("filepath " + filePath);


                //orig = "C:\\dl\\TPC458711_25.pdf";

                if (filePath.Length > 0)
                {

                    string rootPath = "inv" + emailID;

                    System.IO.Directory.CreateDirectory(rootPath);


                    PdfDocument pdfDoc = new PdfDocument(new PdfReader(new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read)));

                    IList<PdfDocument> splitDocuments = new CustomPdfSplitter(pdfDoc, rootPath).SplitByPageCount(1);


                    int pageCount = 0;

                    foreach (PdfDocument doc in splitDocuments)
                    {
                        doc.Close();
                        pageCount++;
                    }

                    pdfDoc.Close();

                    string inputPdf = rootPath + "/splitDocument_" + pageCount + ".pdf";
                    string outputPng = rootPath + "/splitDocument_" + pageCount + ".jpg";
                    string largePdf = rootPath + "/large_" + pageCount + ".pdf";

                    ManipulatePdf(largePdf, inputPdf);


                    using (MagickImage images = new MagickImage())
                    {
                        images.Read(largePdf);
                        images.Format = MagickFormat.Jpeg;
                        images.Resize(new Percentage(150));
                        //images.Scale(new Percentage(400));
                        images.Density = new Density(300);
                        images.Quality = 120;
                        images.Alpha(AlphaOption.Remove);
                        images.Sharpen();
                        images.Interpolate = ImageMagick.PixelInterpolateMethod.Undefined;
                        images.Write(outputPng);
                        images.Dispose();
                    }


                    // Create a new image at the cropped size
                    //doVendorOCR(Vendor.TOPCO, outputPng);

                    Console.WriteLine("vendor " + vendorID);


                    if (v != Vendor.NONE)
                        doVendorOCR(v, outputPng, pageCount, emailID, invoiceID, vendorID);
                    else
                    {
                        using (LocalDatabase ldb = new LocalDatabase(countryID))
                        {
                            sql = " update vendorInvoiceEmails set statusID = " + Convert.ToInt32(VendorInvoiceEmailStatus.FAILED) + ", exceptionMsg = 'No Vendor Match: " + vendorID + "' where id = " + emailID;
                            ldb.insertOrUpdate(sql);
                        }
                    }


                    Directory.Delete(rootPath, true);
                }
            }
            else
            {
                Console.WriteLine("no template found ");

                using (LocalDatabase ldb = new LocalDatabase(countryID))
                {
                    string sql = " update vendorInvoiceEmails set statusID = " + Convert.ToInt32(VendorInvoiceEmailStatus.FAILED) + ",  failedReason = 'No Vendor Template' where id = " + emailID;
                    ldb.insertOrUpdate(sql);
                }
            }
        }


        static void doVendorOCR(Vendor v, string outputPng, int pageCount, int emailID, int invoiceID, int invoiceVendorID)
        {
            if (v == Vendor.TOPCO)
            {

                Image invoiceNumber = Crop(outputPng, 260, 55, 390, 730);
                //invoiceNumber.Save("invoiceNumber.png");

                OCRResult invResult = doOCR(invoiceNumber);

                Image po = Crop(outputPng, 158, 62, 3269, 801);
                //po.Save("poNum.png");

                OCRResult poResult = doOCR(po);

                Image date = Crop(outputPng, 260, 78, 3169, 575);
                //date.Save("date.png");

                OCRResult dateResult = doOCR(date);

                Image amount = Crop(outputPng, 145, 75, 3030, 4117);
                // amount.Save("amount.png");

                OCRResult amountResult = doOCR(amount);

                updateEBSInvoice(cleanOCR(invResult.result), cleanOCR(poResult.result), cleanOCR(dateResult.result), cleanOCR(amountResult.result),"", emailID, invoiceID, invoiceVendorID);

                po.Dispose();
                date.Dispose();
                amount.Dispose();
                invoiceNumber.Dispose();

            }
            else if (v == Vendor.BADGER)
            {


                /*
                var image = new Bitmap(outputPng);
                var ocr = new Tesseract();
                ocr.SetVariable("tessedit_char_whitelist", "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,$-/#&=()\"':?"); // If digit only
                                                                                                                                             //@"C:\OCRTest\tessdata" contains the language package, without this the method crash and app breaks
                ocr.Init(@"C:\OCRTest\tessdata", "eng", true);
                var result = ocr.DoOCR(image, Rectangle.Empty);
                foreach (Word word in result)
                {
                  if (word.contains("aimon"))
                  {
                    Console.WriteLine("" + word.Confidence + " " + word.Text + " " + word.Top + " " + word.Bottom + " " + word.Left + " " + word.Right);
                  }
                }
                */

                int bottomWarningText = 4011;
                int offset = 0;

                string totalBrick = "";

                try
                {
                    using (var engine = new TesseractEngine(ConfigurationManager.AppSettings["tessData"], "eng"))
                    {
                        using (var img = new System.Drawing.Bitmap(outputPng))
                        {

                            using (var pix = PixConverter.ToPix(img))
                            {
                                using (var page = engine.Process(pix))
                                {
                                    var regions = page.GetSegmentedRegions(PageIteratorLevel.Para);


                                    foreach (var region in regions)
                                    {

                                        Image regionCrop = Crop(outputPng, region.Width + 10, region.Height + 10, region.Left - 5, region.Top - 5);
                                        //regionCrop.Save("rCrop.png");

                                        OCRResult regionResult = doOCR(regionCrop);

                                        // if (region.Left > 1650 && region.Top > 3250 && region.Top < 3650)
                                        if (regionResult.result.Contains("Total (USD):") && regionResult.result.Contains("Total Shipped: "))
                                            totalBrick = regionResult.result;

                                    }


                                    //var newHeight = regions[regions.Count - 2];

                                    //offset = newHeight.Top - bottomWarningText;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }





                Image invoiceNumber = Crop(outputPng, 228, 78, 3203, 468);
                invoiceNumber.Save("invoiceNumber.png");

                OCRResult invResult = doOCR(invoiceNumber);

                Image po = Crop(outputPng, 276, 62, 3155, 797);
                po.Save("poNum.png");

                OCRResult poResult = doOCR(po);

                Image date = Crop(outputPng, 276, 62, 3153, 583);
                date.Save("date.png");

                OCRResult dateResult = doOCR(date);

                string amountRes = "";
                string taxRes = "";
                string shippedRes = "";
                string totalRes = "";



                if (totalBrick.Length > 0)
                {

                    //Sales Total: 127,776.14
                    //Tax Total: 6,388.81
                    //Total Shipped: 1,322.8712 Total(USD): 134,164.95
                    try
                    {
                        amountRes = totalBrick.Substring(totalBrick.IndexOf("Sales Total:"), totalBrick.IndexOf("\n", totalBrick.IndexOf("Sales Total:")) - totalBrick.IndexOf("Sales Total:")).Replace("Sales Total:", "").Trim();
                    }
                    catch { }

                    try
                    {
                        taxRes = totalBrick.Substring(totalBrick.IndexOf("Tax Total:"), totalBrick.IndexOf("\n", totalBrick.IndexOf("Tax Total:")) - totalBrick.IndexOf("Tax Total:")).Replace("Tax Total:", "").Trim();
                    }
                    catch { }


                    try
                    {
                        shippedRes = totalBrick.Substring(totalBrick.IndexOf("Total Shipped:"), totalBrick.IndexOf("Total (USD):") - totalBrick.IndexOf("Total Shipped:")).Replace("Total Shipped:", "").Trim();


                        double shippedD = Convert.ToDouble(shippedRes);

                        double mtToSt = 0.9071858188712794;

                        shippedRes = Convert.ToDouble(shippedD * mtToSt).ToString();


                    }
                    catch { }


                    try
                    {
                        totalRes = totalBrick.Substring(totalBrick.IndexOf("Total (USD):"), totalBrick.IndexOf("\n", totalBrick.IndexOf("Total (USD):")) - totalBrick.IndexOf("Total (USD):")).Replace("Total (USD):", "").Trim();
                    }
                    catch { }


                }


                Console.WriteLine("invResult.result " + invResult.result);

                updateEBSInvoice(cleanOCR(invResult.result), cleanOCR(poResult.result), cleanOCR(dateResult.result), cleanOCR(amountRes), "", emailID, invoiceID, invoiceVendorID, cleanOCR(shippedRes), cleanOCR(taxRes), cleanOCR(totalRes), 1);


                po.Dispose();
                date.Dispose();
                invoiceNumber.Dispose();

            }
            else if (v == Vendor.GREGG)
            {



                Image invoiceNumber = Crop(outputPng, 467, 76, 2930, 1453);
                //invoiceNumber.Save("invoiceNumber.png");

                OCRResult invResult = doOCR(invoiceNumber);

                Image po = Crop(outputPng, 290, 75, 1294, 1586);
                //po.Save("poNum.png");

                OCRResult poResult = doOCR(po);

                Image date = Crop(outputPng, 325, 65, 218, 1586);
                //date.Save("date.png");

                OCRResult dateResult = doOCR(date);


                Image amount = Crop(outputPng, 480, 80, 3030, 4312);
                //amount.Save("amount.png");

                OCRResult amountResult = doOCR(amount);

                if (amountResult.result.ToLower().Contains("continued"))
                {
                    amountResult.result = "";
                }



                Image taxes = Crop(outputPng, 480, 80, 3014, 4194);
                //amount.Save("amount.png");

                OCRResult taxRes = doOCR(taxes);

                Image preTax = Crop(outputPng, 516, 61, 3011, 4128);
                //amount.Save("amount.png");

                OCRResult preTaxRes = doOCR(preTax);



                Console.WriteLine("invResult.result " + invResult.result);

                updateEBSInvoice(cleanOCR(invResult.result), cleanOCR(poResult.result), cleanOCR(dateResult.result), cleanOCR(preTaxRes.result), cleanOCR(taxRes.result), emailID, invoiceID, invoiceVendorID);


                po.Dispose();
                date.Dispose();
                amount.Dispose();
                invoiceNumber.Dispose();

            }
        }

        static string cleanOCR(string inc)
        {
            string retVal = inc;

            retVal = retVal.Replace("—", "");
            retVal = retVal.Replace("\n", "");



            return retVal;
        }

        static bool updateEBSInvoice(string invoiceNumber, string poNumber, string date, string amount, string invTax, int emailID, int invoiceID, int invoiceVendorID, string quantity = "", string tax = "", string totalTotal = "", int isSandInvoice = 0)
        {

            bool success = true;



            int poID = 0;
            double poAmount = 0;
            int poVendorID = 0;

            string sql = "";


            if (isSandInvoice == 0)
            {

                try
                {

                    if (poNumber.Length > 0)
                    {
                        sql = "select * from purchaseorders where poNumber = '" + poNumber + "' AND isClosed = 0";

                        using (LocalDatabase ldb = new LocalDatabase(countryID))
                        {
                            ldb.myCommandLocal.CommandText = sql;

                            ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                            if (ldb.myReaderLocal.Read())
                            {
                                poID = Convert.ToInt32(ldb.myReaderLocal["id"].ToString());
                                poAmount = Convert.ToDouble(ldb.myReaderLocal["amount"].ToString());
                                poVendorID = Convert.ToInt32(ldb.myReaderLocal["poVendorID"].ToString());

                            }

                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
            else if (isSandInvoice == 1)
            {

                try
                {
                    sql = "select * from orders where internalID = '" + poNumber + "' and vendorid = " + invoiceVendorID + " and status = 'Open'";


                    using (LocalDatabase ldb = new LocalDatabase(countryID))
                    {
                        ldb.myCommandLocal.CommandText = sql;

                        ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                        if (ldb.myReaderLocal.Read())
                        {
                            poID = Convert.ToInt32(ldb.myReaderLocal["id"].ToString());
                            poVendorID = Convert.ToInt32(ldb.myReaderLocal["vendorID"].ToString());
                        }


                    }
                }
                catch (Exception ex)
                {

                }


                int isRail = 0;

                if (invoiceVendorID == Convert.ToInt32(Vendor.BADGER))
                    isRail = 0;


                if (poID > 0)
                {
                    try
                    {
                        using (LocalDatabase db = new LocalDatabase(countryID))
                        {
                            sql = " update invoices set orderID = " + poID + ", isOrder = 1, isRail = " + isRail + " where id = " + invoiceID;
                            db.insertOrUpdate(sql);
                        }
                    }
                    catch (Exception e1)
                    {

                    }
                }

            }

            int statusID = 3; //failed by default

            bool allGood = true;

            string failedReason = "null";



            bool dupInvNumber = false;

            if (invoiceNumber.Length > 0)
            {
                using (LocalDatabase ldb = new LocalDatabase(countryID))
                {
                    ldb.myCommandLocal.CommandText = "select * from invoices where invoiceNumber =  '" + invoiceNumber.Replace("'", "''") + "' and poVendorID = " + invoiceVendorID;

                    ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                    if (ldb.myReaderLocal.Read())
                    {
                        dupInvNumber = true;
                    }

                }
            }



            using (LocalDatabase db = new LocalDatabase(countryID))
            {

                if (!dupInvNumber)
                {
                    try
                    {
                        sql = " update invoices set invoiceNumber = '" + invoiceNumber.Replace("'", "''") + "' where id = " + invoiceID;
                        db.insertOrUpdate(sql);
                    }
                    catch (Exception e1)
                    {
                        allGood = false;
                    }
                }
               


                //update my currency

                string currency = "";

                try
                {
                    using (LocalDatabase ldb = new LocalDatabase(countryID))
                    {
                        ldb.myCommandLocal.CommandText = "select currency from povendors where id = " + invoiceVendorID;

                        ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                        if (ldb.myReaderLocal.Read())
                        {
                            currency = ldb.myReaderLocal["currency"].ToString();
                        }

                    }

                    if (currency.Length > 0)
                    {
                        sql = " update invoices set currency = '" + currency + "' where id = " + invoiceID;
                        db.insertOrUpdate(sql);
                    }

                }
                catch (Exception e1)
                {

                }


                try
                {
                    sql = " update invoices set invoiceDate = '" + date.Replace("'", "''") + "' where id = " + invoiceID;
                    db.insertOrUpdate(sql);
                }
                catch (Exception e2)
                {
                    allGood = false;
                }

                try
                {
                    sql = " update invoices set amount = '" + amount.Replace("'", "''").Replace(",", "") + "' where id = " + invoiceID;
                    db.insertOrUpdate(sql);
                }
                catch (Exception e3)
                {
                    allGood = false;
                }


                if (invTax.Length > 0)
                {
                    try
                    {
                        sql = " update invoices set gst = '" + invTax.Replace("'", "''").Replace(",", "") + "' where id = " + invoiceID;
                        db.insertOrUpdate(sql);
                    }
                    catch (Exception e3)
                    {
                      
                    }
                }

                try
                {
                    if (poAmount != Convert.ToDouble(amount))
                    {
                        allGood = false;
                        failedReason = "'" + FailedReasons.PO_AMOUNT_MISMATCH + "'";
                    }
                }
                catch (Exception e4)
                {
                    allGood = false;
                }


                if (poID == 0) //couldn't link my PO
                {
                    allGood = false;
                    failedReason = "'" + FailedReasons.CANT_MATCH_PO.ToString() + "'";
                }
                else if (poVendorID != invoiceVendorID)
                {
                    allGood = false;
                    failedReason = "'" + FailedReasons.PO_VENDOR_MISMATCH.ToString() + "'";
                }
                else if(dupInvNumber)
                {
                    allGood = false;
                    failedReason = "'" + FailedReasons.DUPLICATE + "'";
                }
                else
                {


                    if (isSandInvoice == 0)
                    {

                        sql = "insert into relInvoicePurchaseOrders(invoiceID, poID) values(" + invoiceID + "," + poID + ")";
                        db.insertOrUpdate(sql);

                        //bring PO lines into invoice

                        sql = "insert into purchaseorderparts(poID, isInvoice, partNumber, partDesc, equipmentID, qty, price, amount, afe,  afeString, afeID, glTypeID, pst, cost, dept, jobID, numLoads, mt, costPer, glCodeID, copiedFromOld, loadedAt, location, gst, regionID, invoiceDesc, spreadGLCode, spreadGLCodeID, shipmentItemID, standbyHrs, coilStringID, multiUnitPriceSplit, multiUnitExtendedSplit, sandHaulerLoadID )";
                        sql = sql + " select " + invoiceID + ", 1, partNumber, partDesc, equipmentID, qty,  price, amount, afe,  afeString, afeID, glTypeID, pst, cost, dept, jobID, numLoads, mt, costPer, glCodeID, copiedFromOld, loadedAt, location, gst, regionID, invoiceDesc, spreadGLCode, spreadGLCodeID, shipmentItemID, standbyHrs, coilStringID, multiUnitPriceSplit, multiUnitExtendedSplit, sandHaulerLoadID ";
                        sql = sql + " from purchaseorderparts where poID= " + poID + " and isInvoice = 0 and isdeleted = 0 ";
                        db.insertOrUpdate(sql);

                        sql = "update purchaseorders set isclosed = 1 where id = " + poID;
                        db.insertOrUpdate(sql);

                        using (LocalDatabase ldb = new LocalDatabase(countryID))
                        {
                            sql = "select sum(amount)  as poTotal from PurchaseOrderParts where  isinvoice = 0 and isDeleted = 0 and  poid =   " + poID;

                            ldb.myCommandLocal.CommandText = sql;

                            ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                            double newInvAmount = 0;

                            if (ldb.myReaderLocal.Read())
                            {
                                newInvAmount = Convert.ToDouble(ldb.myReaderLocal["poTotal"].ToString());

                              sql = "update invoices set amount = " + newInvAmount + " where id = " + invoiceID;
                              db.insertOrUpdate(sql);
                            }

                        }

                    }
                    else if (isSandInvoice == 1)
                    {

                        string partNumber = "";
                        string partDesc = "";

                        try
                        {
                            using (LocalDatabase ldb = new LocalDatabase(countryID))
                            {
                                sql = "select top 1 pb.code, pb.description, oi.* from orderedItems oi inner join pricebook pb on pb.id = oi.PriceBookID where oi.orderID =  " + poID;

                                ldb.myCommandLocal.CommandText = sql;

                                ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                                if (ldb.myReaderLocal.Read())
                                {
                                    partNumber = ldb.myReaderLocal["code"].ToString();
                                    partDesc = "PO" + poNumber + " " + ldb.myReaderLocal["description"].ToString();
                                }

                            }
                        }
                        catch (Exception ex)
                        {

                        }

                        double invQty = 0;
                        try
                        {
                            invQty = Convert.ToDouble(quantity);
                        }
                        catch (Exception ex)
                        {

                        }

                        double invPrice = 0;

                        double pricePer = 0;

                        try
                        {
                            invPrice = Convert.ToDouble(amount);

                            pricePer = invPrice / invQty;

                        }
                        catch (Exception ex)
                        {

                        }

                        double gst = 0;

                        try
                        {
                            gst = Convert.ToDouble(tax);



                        }
                        catch (Exception ex)
                        {

                        }


                        sql = "insert into purchaseorderparts(poID, isInvoice, partNumber, partDesc, equipmentID, qty, price, amount, afe,  afeString, afeID, glTypeID, pst, cost, dept, jobID, numLoads, mt, costPer, glCodeID, copiedFromOld, loadedAt, location, gst, regionID, invoiceDesc, spreadGLCode, spreadGLCodeID, shipmentItemID, standbyHrs, coilStringID, multiUnitPriceSplit, multiUnitExtendedSplit, sandHaulerLoadID ) values (";
                        sql = sql + $@"{invoiceID}, 1,  '{partNumber}', '{partDesc}', null, {invQty}, {pricePer}, {invPrice}, 0 , null, null,  4,  0, null, null, null, null, null, null, 305, null, null, null, {gst}, null, null, 500, 25, null, null,null,null,null,null)";
                        db.insertOrUpdate(sql);

                    }

                    failedReason = "'" + FailedReasons.PO_MATCHED.ToString() + "'";

                }

            }

            if (allGood)
                statusID = 2;

            using (LocalDatabase db = new LocalDatabase(countryID))
            {
                sql = " update vendorInvoiceEmails set statusID = " + statusID + ", failedReason = " + failedReason + " where id = " + emailID;
                db.insertOrUpdate(sql);
            }

            string vendorName = "";
            if (invoiceVendorID == Convert.ToInt32(Vendor.GREGG))
                vendorName = "Gregg Distributors LP";
            else if (invoiceVendorID == Convert.ToInt32(Vendor.TOPCO))
                vendorName = "Topco Oilsite Products Limited";
            else if (invoiceVendorID == Convert.ToInt32(Vendor.BADGER))
                vendorName = "Badger Mining Corp.";



            string internalInv = "";

            try
            {
                using (LocalDatabase ldb = new LocalDatabase(countryID))
                {
                    sql = "select prefixedInvoiceID from invoices where id =  " + invoiceID;

                    ldb.myCommandLocal.CommandText = sql;

                    ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                    if (ldb.myReaderLocal.Read())
                    {
                        internalInv = ldb.myReaderLocal["prefixedInvoiceID"].ToString();
                    }

                }
            }
            catch (Exception ex)
            {

            }


            sendEmail("An automated invoice " + internalInv + " has been processed for " + vendorName + " ", "Please click <a href='https://element.ripplegroup.ca/elementAdmin/InvoiceAddEdit2.asp?Action=Edit&gr=Invoices&ID=" + invoiceID + "'>here</a> to view ", GetInvoiceAlertEmailList());


            return success;
        }

        static OCRResult doOCR(Image i)
        {
            OCRResult retVal = new OCRResult(0, "");

            using (var engine = new TesseractEngine(ConfigurationManager.AppSettings["tessData"], "eng"))
            {
                using (var img = new System.Drawing.Bitmap(i))
                {
                    using (var pix = PixConverter.ToPix(img))
                    {
                        using (var page = engine.Process(pix))
                        {
                            retVal.confidence = page.GetMeanConfidence();
                            retVal.result = page.GetText();
                        }
                    }
                }
            }

            return retVal;
        }

        static string GetInvoiceAlertEmailList()
        {
            string emailList = "";

            using (LocalDatabase ldb = new LocalDatabase(countryID))
            {
                ldb.myCommandLocal.CommandText = "select thelist from emaillists where name = 'OCRInvoiceAlerts'";

                ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                if (ldb.myReaderLocal.Read())
                {
                    emailList = ldb.myReaderLocal["thelist"].ToString();
                }

            }


            return emailList;

        }

        static void ManipulatePdf(string dest, string src)
        {
            PdfDocument pdfDoc = new PdfDocument(new PdfReader(src), new PdfWriter(dest));

            for (int p = 1; p <= pdfDoc.GetNumberOfPages(); p++)
            {
                PdfPage page = pdfDoc.GetPage(p);
                iText.Kernel.Geom.Rectangle media = page.GetCropBox();
                if (media == null)
                {
                    media = page.GetMediaBox();
                }

                // Shrink the page to 50%
                iText.Kernel.Geom.Rectangle crop = new iText.Kernel.Geom.Rectangle(0, 0, media.GetWidth() * 4, media.GetHeight() * 4);
                page.SetMediaBox(crop);
                page.SetCropBox(crop);

                // The content, placed on a content stream before, will be rendered before the other content
                // and, therefore, could be understood as a background (bottom "layer")
                new PdfCanvas(page.NewContentStreamBefore(),
                        page.GetResources(), pdfDoc).WriteLiteral("\nq 4 0 0 4 0 0 cm\nq\n");

                // The content, placed on a content stream after, will be rendered after the other content
                // and, therefore, could be understood as a foreground (top "layer")
                new PdfCanvas(page.NewContentStreamAfter(),
                        page.GetResources(), pdfDoc).WriteLiteral("\nQ\nQ\n");
            }

            pdfDoc.Close();
        }

        public static void sendEmail(string subject, string body, string to = "dave@ripplegroup.ca")
        {
            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["smtpServer"])
            {
                Port = 587,
                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["smtpLogin"], ConfigurationManager.AppSettings["smtpPassword"]),
                EnableSsl = true,
            };

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(ConfigurationManager.AppSettings["smtpLogin"]);
            msg.To.Add(to);
            msg.Subject = subject;
            msg.IsBodyHtml = true;
            msg.Body = body;

            smtpClient.Send(msg);
        }

        public static Image Crop(string img, int width, int height, int x, int y)
        {

            Image image = Image.FromFile(img);
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            bmp.SetResolution(80, 60);

            Graphics gfx = Graphics.FromImage(bmp);
            gfx.SmoothingMode = SmoothingMode.AntiAlias;
            gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
            gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
            gfx.DrawImage(image, new System.Drawing.Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);


            return bmp;
        }


        class CustomPdfSplitter : PdfSplitter
        {
            int _partNumber = 1;
            string path = "";

            public CustomPdfSplitter(PdfDocument pdfDocument, string _path) : base(pdfDocument)
            {
                path = _path;
            }

            protected override PdfWriter GetNextPdfWriter(PageRange documentPageRange)
            {
                try
                {
                    return new PdfWriter(path + "/splitDocument_" + _partNumber++ + ".pdf");
                }
                catch (FileNotFoundException e)
                {
                    throw new SystemException();
                }
            }
        }
    }
}
